OPTIONS = -g -Wall
OBJ     = main.o func.o

main:  $(OBJ)
	   gcc -o main $(OPTIONS) main.o func.o   #Actual Program

main.o:  main.c TypeDefs.h                            #The main
	     gcc -c $(OPTIONS) main.c

func.o:  func.c TypeDefs.h                        #Utilities
	     gcc -c $(OPTIONS) func.c

clean:
	   rm $(OBJ) main