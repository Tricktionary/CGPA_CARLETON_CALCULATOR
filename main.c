#include "TypeDefs.h"

int main(){
	
	int prompt = 1;
	char letterGrade[MAX_STR];
	float gradeValue;

	int weightChoice;
	float currentWeight;

	float currentValue;

	float totalWeight= 0;
	float totalGPA = 0;

	float gpa = 0;

	while(prompt == 1){
		printf("Do you want to add a grade to your GPA (Input 0 for NO | Input 1 for YES) : \n ");
		readInt(&prompt);
		
		if(prompt == 0){
			break;
		}
		
		printf("Please input the Letter grade received:\n");
		readString(letterGrade);

		gradeValue = gradeConvert(letterGrade);

		printf("Please input the weight of the grade (Input 0 for 0.5 | Input 1 for 1)  :\n");
		readInt(&weightChoice);

		//Calculation

		currentWeight = weightChange(weightChoice);

		totalWeight = totalWeight + currentWeight;

		currentValue = currentWeight * gradeValue;

		totalGPA = totalGPA + currentValue;
	}

	gpa = totalGPA / totalWeight;

	printf("Your GPA is a %0.1f\n",gpa);
	
	return(0);
}

 