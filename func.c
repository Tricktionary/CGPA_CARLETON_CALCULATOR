#include "TypeDefs.h"

/*
	Function: weightChange
	Purpose : Receives a weight and changes the value based on it
	input   : weight Choice
	output  : actual weight
*/
float weightChange(int weightChoice){
	if (weightChoice == 1){
		return(1);
	}
	if (weightChoice == 0){
		return(0.5);
	}
	else{
		return(-1);
	}
}
/*
	Function: gradeConvert
	Purpose : Receives a letter grade an converts it to an actual value
	input   : Letter Grade
	output  : Grade value
*/
float gradeConvert(char* grade){
	if(strcmp(grade,"A+") == 0){
		return 12;
	}
	if(strcmp(grade,"A") == 0){
		return 11;
	}
	if(strcmp(grade,"A-") == 0){
		return 10;
	}
	if(strcmp(grade,"B+") == 0){
		return 9;
	}
	if(strcmp(grade,"B") == 0){
		return 8;
	}
	if(strcmp(grade,"B-") == 0){
		return 7;
	}
	if(strcmp(grade,"C+") == 0){
		return 6;
	}
	if(strcmp(grade,"C") == 0){
		return 5;
	}
	if(strcmp(grade,"C-") == 0){
		return 4;
	}
	if(strcmp(grade,"D+") == 0){
		return 3;
	}
	if(strcmp(grade,"D") == 0){
		return 2;
	}
	if(strcmp(grade,"D-") == 0){
		return 1;
	}
	if(strcmp(grade,"F") == 0){
		return 0;
	}
	else{
		return -1;
	}
}

/*
    Function:  readString
    Purpose:   reads a string from standard input
        out:   string read in from the user
               (must be allocated in calling function)
*/
void readString(char *str)
{
  char tmpStr[MAX_STR];

  fgets(tmpStr, sizeof(tmpStr), stdin);
  tmpStr[strlen(tmpStr)-1] = '\0';
  strcpy(str, tmpStr);
}


/*
    Function:  readInt
    Purpose:   reads an integer from standard input
        out:   integer read in from the user
               (must be allocated in calling function)
*/
void readInt(int *x)
{
  char str[MAX_STR];

  readString(str);
  sscanf(str, "%d", x);
}