#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_STR 32

void readString(char*);
void readInt(int*);
float gradeConvert(char*);
float weightChange(int);
